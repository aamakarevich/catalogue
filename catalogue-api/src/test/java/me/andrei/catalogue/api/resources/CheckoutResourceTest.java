package me.andrei.catalogue.api.resources;

import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import me.andrei.catalogue.domain.model.CatalogueItem;
import me.andrei.catalogue.domain.provider.CatalogueProvider;
import org.junit.jupiter.api.Test;

import java.util.List;

import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.eclipse.jetty.http.HttpStatus.OK_200;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class CheckoutResourceTest {

    private final Context ctx = mock(Context.class);
    private final CatalogueProvider catalogueProvider = mock(CatalogueProvider.class);
    private final CheckoutResource checkoutResource = new CheckoutResource(catalogueProvider);

    @Test
    public void should_calculate_price_when_all_items_found_in_catalogue() {
        // given
        given(ctx.bodyAsClass(Long[].class)).willReturn(new Long[]{1L, 2L, 3L, 1L});
        var watch1 = new CatalogueItem(1, "Rolex", money(100), discount(2, money(150)));
        var watch2 = new CatalogueItem(2, "Michael Kors", money(80), discount(2, money(100)));
        var watch3 = new CatalogueItem(3, "Swatch", money(200));
        given(catalogueProvider.findItemsBy(List.of(1L, 2L, 3L, 1L))).willReturn(List.of(
            watch1, watch2, watch3
        ));

        // when
        checkoutResource.checkout(ctx);

        // then
        verify(ctx).status(OK_200);
        verify(ctx).json(money(430));
    }

    @Test
    public void should_throw_an_exception_when_some_items_not_found_in_catalogue() {
        // given
        given(ctx.bodyAsClass(Long[].class)).willReturn(new Long[]{1L, 2L, 3L});
        var watch = new CatalogueItem(1, "Rolex", money(100), discount(2, money(150)));
        given(catalogueProvider.findItemsBy(List.of(1L, 2L, 3L, 1L))).willReturn(List.of(watch));

        // when
        var thrown = assertThatThrownBy(() -> checkoutResource.checkout(ctx));

        // then
        thrown
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("some of the purchased items are not in catalogue");
    }

    @Test
    public void should_not_omit_an_exception_when_request_body_is_invalid() {
        // given
        given(ctx.bodyAsClass(Long[].class)).willThrow(new BadRequestResponse("can not deserialize"));

        // when
        var thrown = assertThatThrownBy(() -> checkoutResource.checkout(ctx));

        // then
        thrown
            .isInstanceOf(BadRequestResponse.class)
            .hasMessage("can not deserialize");
    }
}
