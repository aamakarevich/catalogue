package me.andrei.catalogue.api;

import me.andrei.catalogue.domain.model.CatalogueItem;
import me.andrei.catalogue.domain.provider.InMemoryCatalogueProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.util.List;

public abstract class FunctionalSpecBase {

    private final InMemoryCatalogueProvider catalogueProvider = new InMemoryCatalogueProvider(List.of());

    protected final Application app = new Application(new DependencyContext(catalogueProvider));
    protected int port;

    @BeforeEach
    void before() {
        app.start(0);
        port = app.port();
    }

    @AfterEach
    void afterEach() {
        catalogueProvider.clear();
        app.stop();
    }

    protected void givenExists(CatalogueItem item) {
        catalogueProvider.add(item);
    }
}
