package me.andrei.catalogue.api;

import me.andrei.catalogue.domain.model.CatalogueItem;
import org.junit.jupiter.api.Test;

import static kong.unirest.Unirest.post;
import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.INTERNAL_SERVER_ERROR_500;
import static org.eclipse.jetty.http.HttpStatus.OK_200;

class CheckoutResourceSpec extends FunctionalSpecBase {

    @Test
    void should_return_calculated_price_when_all_items_found() {
        // given
        givenExists(new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))));
        givenExists(new CatalogueItem(2, "Casio", money(150)));
        var purchasedItemsIds = new long[]{1, 2, 1, 2, 2, 2};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(purchasedItemsIds)
            .asString();

        // then
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getStatus()).isEqualTo(OK_200);
        assertThat(response.getBody()).isEqualTo("{\"amount\":800}");
    }

    @Test
    void should_return_calculated_price_when_discount_should_be_applied() {
        // given
        givenExists(new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))));
        givenExists(new CatalogueItem(2, "Casio", money(150)));
        var purchasedItemsIds = new long[]{1, 2, 1, 2, 1, 2};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(purchasedItemsIds)
            .asString();

        // then
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getStatus()).isEqualTo(OK_200);
        assertThat(response.getBody()).isEqualTo("{\"amount\":650}");
    }

    @Test
    void should_return_calculated_price_when_discount_should_be_applied_several_times() {
        // given
        givenExists(new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))));
        givenExists(new CatalogueItem(2, "Casio", money(150)));
        givenExists(new CatalogueItem(3, "Swatch", money(150), discount(1, money(80))));
        var purchasedItemsIds = new long[]{1, 2, 1, 2, 1, 1, 1, 1, 1, 3, 3};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(purchasedItemsIds)
            .asString();

        // then
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getStatus()).isEqualTo(OK_200);
        assertThat(response.getBody()).isEqualTo("{\"amount\":960}");
    }

    @Test
    void should_return_zero_price_when_no_items_are_being_purchased() {
        // given
        givenExists(new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))));
        givenExists(new CatalogueItem(2, "Casio", money(150)));
        var purchasedItemsIds = new long[]{};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(purchasedItemsIds)
            .asString();

        // then
        assertThat(response.isSuccess()).isTrue();
        assertThat(response.getStatus()).isEqualTo(OK_200);
        assertThat(response.getBody()).isEqualTo("{\"amount\":0}");
    }

    @Test
    void should_return_internal_server_error_when_some_of_the_items_are_not_in_catalogue() {
        // given
        givenExists(new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))));
        givenExists(new CatalogueItem(2, "Casio", money(150)));
        var purchasedItemsIds = new long[]{1, 2, 1, 4, 3, 5};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(purchasedItemsIds)
            .asString();

        // then
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getStatus()).isEqualTo(INTERNAL_SERVER_ERROR_500);
        assertThat(response.getBody()).isEqualTo("some of the purchased items are not in catalogue");
    }

    @Test
    void should_return_bad_request_when_request_body_is_invalid() {
        // given
        var requestBody = new String[]{"Rolex", "Casio"};

        // when
        var response = post("http://localhost:" + port + "/checkout")
            .body(requestBody)
            .asString();

        // then
        assertThat(response.isSuccess()).isFalse();
        assertThat(response.getStatus()).isEqualTo(BAD_REQUEST_400);
        assertThat(response.getBody()).isEqualTo("Couldn't deserialize body to Long[]");
    }
}
