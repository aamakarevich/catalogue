package me.andrei.catalogue.api;

import me.andrei.catalogue.domain.provider.CatalogueProvider;

public class DependencyContext {
    public final CatalogueProvider catalogueProvider;

    public DependencyContext(CatalogueProvider catalogueProvider) {
        this.catalogueProvider = catalogueProvider;
    }
}
