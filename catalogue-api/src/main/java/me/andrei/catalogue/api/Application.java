package me.andrei.catalogue.api;

import io.javalin.Javalin;
import io.javalin.http.BadRequestResponse;
import me.andrei.catalogue.api.resources.CheckoutResource;
import me.andrei.catalogue.common.exception.ValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.Integer.parseInt;
import static me.andrei.catalogue.domain.provider.InMemoryCatalogueProvider.DEFAULT_IN_MEMORY_CATALOGUE_PROVIDER;
import static org.eclipse.jetty.http.HttpStatus.BAD_REQUEST_400;
import static org.eclipse.jetty.http.HttpStatus.INTERNAL_SERVER_ERROR_500;

public class Application {

    private static final Logger LOG = LoggerFactory.getLogger(Application.class);
    private static final int DEFAULT_PORT = 8080;

    public static void main(String[] args) {
        final var port = args.length == 1 ? parseInt(args[0]) : DEFAULT_PORT;
        new Application(new DependencyContext(DEFAULT_IN_MEMORY_CATALOGUE_PROVIDER))
            .start(port);
    }

    private final DependencyContext deps;
    private final Javalin server;

    public Application(DependencyContext deps) {
        this.deps = deps;
        this.server = Javalin.create(config -> config.defaultContentType = "application/json");
    }

    public void start(int port) {
        initializeRoutes();
        initializeErrorHandlers();
        server.start(port);
    }

    private void initializeRoutes() {
        LOG.info("Initializing routes");
        server.routes(() -> new CheckoutResource(deps.catalogueProvider).addEndpoints());
    }

    private void initializeErrorHandlers() {
        LOG.info("Initializing error handlers");
        server
            .exception(ValidationException.class, (ex, ctx) -> {
                ctx.status(BAD_REQUEST_400);
                ctx.result(ex.getMessage());
            })
            .exception(IllegalStateException.class, (ex, ctx) -> {
                ctx.status(INTERNAL_SERVER_ERROR_500);
                ctx.result(ex.getMessage());
            })
            .exception(BadRequestResponse.class, (ex, ctx) -> {
                ctx.status(BAD_REQUEST_400);
                ctx.result(ex.getMessage());
            });
    }

    public int port() {
        return server.port();
    }

    public void stop() {
        server.stop();
    }
}
