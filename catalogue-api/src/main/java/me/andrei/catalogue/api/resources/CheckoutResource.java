package me.andrei.catalogue.api.resources;

import io.javalin.apibuilder.EndpointGroup;
import io.javalin.http.Context;
import me.andrei.catalogue.domain.model.Money;
import me.andrei.catalogue.domain.provider.CatalogueProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.javalin.apibuilder.ApiBuilder.post;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toMap;
import static me.andrei.catalogue.common.checks.Checks.checkThat;

public class CheckoutResource implements EndpointGroup {

    private static final Logger LOG = LoggerFactory.getLogger(CheckoutResource.class);

    private final CatalogueProvider catalogueProvider;

    public CheckoutResource(CatalogueProvider catalogueProvider) {
        this.catalogueProvider = catalogueProvider;
    }

    @Override
    public void addEndpoints() {
        post("checkout", this::checkout);
        LOG.info("Initialized CheckoutResource");
    }

    public void checkout(Context ctx) {
        final var checkoutItemsIds = asList(ctx.bodyAsClass(Long[].class));

        final var catalogueItems = catalogueProvider.findItemsBy(checkoutItemsIds).stream()
            .collect(toMap(item -> item.id, item -> item));

        checkThat(catalogueItems.keySet().containsAll(checkoutItemsIds), "some of the purchased items are not in catalogue");

        final var itemsByCount = checkoutItemsIds.stream()
            .collect(groupingBy(catalogueItems::get, counting()));

        final var price = itemsByCount.entrySet().stream()
            .map(entry -> entry.getKey().priceFor(entry.getValue()))
            .reduce(Money.zero(), Money::add);

        ctx.status(200);
        ctx.json(price);
    }
}
