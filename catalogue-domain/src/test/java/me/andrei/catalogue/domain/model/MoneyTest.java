package me.andrei.catalogue.domain.model;

import me.andrei.catalogue.common.exception.ValidationException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class MoneyTest {

    @ParameterizedTest(name = "should_create_money_when_price_is_not_negative_{index}()")
    @ValueSource(longs = {0, 1, 42, 150})
    void should_create_money_when_amount_is_not_negative(long amount) {
        // given
        // when
        var result = money(amount);

        // then
        assertThat(result.amount).isEqualTo(amount);
    }

    @ParameterizedTest(name = "should_not_create_money_when_amount_is_negative_{index}()")
    @ValueSource(longs = {-1, -42, -150})
    void should_not_create_money_when_amount_is_negative(long amount) {
        // given
        // when
        var thrown = assertThatThrownBy(() -> money(amount));

        // then
        thrown
            .isInstanceOf(ValidationException.class)
            .hasMessage("amount must be greater than or equal to 0, but is %s", amount);
    }
}
