package me.andrei.catalogue.domain.model;

import me.andrei.catalogue.common.exception.ValidationException;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class DiscountTest {

    @ParameterizedTest(name = "should_create_discount_when_count_is_positive_{index}()")
    @ValueSource(longs = {1, 42, 150})
    void should_create_discount_when_count_is_positive(long discountCount) {
        // given
        // when
        var result = discount(discountCount, money(1));

        // then
        assertThat(result.count).isEqualTo(discountCount);
        assertThat(result.price).isEqualTo(money(1));
    }

    @ParameterizedTest(name = "should_not_create_discount_when_count_is_not_positive_{index}()")
    @ValueSource(longs = {0, -1, -42, -150})
    void should_not_create_discount_when_count_is_not_positive(long discountCount) {
        // given
        // when
        var thrown = assertThatThrownBy(() -> discount(discountCount, money(1)));

        // then
        thrown
            .isInstanceOf(ValidationException.class)
            .hasMessage("count must be greater than 0, but is %s", discountCount);
    }
}
