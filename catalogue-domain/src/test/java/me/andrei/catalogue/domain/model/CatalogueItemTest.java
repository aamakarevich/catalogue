package me.andrei.catalogue.domain.model;

import org.junit.jupiter.api.Test;

import java.util.Optional;

import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class CatalogueItemTest {

    @Test
    public void should_create_item_with_discount_that_gives_lower_price() {
        // given
        var discount = discount(5, money(49));

        // when
        var item = new CatalogueItem(1, "Casio", money(10), discount);

        // then
        assertThat(item.discount).isEqualTo(Optional.of(discount));
    }

    @Test
    public void should_not_create_item_with_discount_that_gives_same_price() {
        // given
        var discount = discount(5, money(50));

        // when
        var exception = assertThatThrownBy(() -> new CatalogueItem(1, "Casio", money(10), discount));

        // then
        exception
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("discount price should be less than normal price");
    }

    @Test
    public void should_not_create_item_with_discount_that_gives_higher_price() {
        // given
        var discount = discount(5, money(51));

        // when
        var exception = assertThatThrownBy(() -> new CatalogueItem(1, "Casio", money(10), discount));

        // then
        exception
            .isInstanceOf(IllegalStateException.class)
            .hasMessage("discount price should be less than normal price");
    }
}
