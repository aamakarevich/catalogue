package me.andrei.catalogue.domain.provider;

import me.andrei.catalogue.domain.model.CatalogueItem;
import org.junit.jupiter.api.Test;

import java.util.List;

import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;

class InMemoryCatalogueProviderTest {

    private final CatalogueItem watch1 = new CatalogueItem(1, "Rolex", money(100), discount(3, money(200)));
    private final CatalogueItem watch2 = new CatalogueItem(2, "Michael Kors", money(100), discount(2, money(120)));
    private final CatalogueItem watch3 = new CatalogueItem(3, "Swatch", money(50));
    private final CatalogueItem watch4 = new CatalogueItem(4, "Casio", money(30));

    private final InMemoryCatalogueProvider catalogueProvider = new InMemoryCatalogueProvider(List.of(
        watch1, watch2, watch3, watch4
    ));

    @Test
    public void should_find_existent_items_when_looking_for_them() {
        // given
        var idsOfItemsToFind = List.of(1L, 2L, 3L, 4L);

        // when
        var foundItems = catalogueProvider.findItemsBy(idsOfItemsToFind);

        // then
        assertThat(foundItems).containsExactlyInAnyOrder(watch1, watch2, watch3, watch4);
    }

    @Test
    public void should_find_existent_items_when_looking_for_existent_and_non_existent() {
        // given
        var idsOfItemsToFind = List.of(4L, 2L, 42L, 7L);

        // when
        var foundItems = catalogueProvider.findItemsBy(idsOfItemsToFind);

        // then
        assertThat(foundItems).containsExactlyInAnyOrder(watch2, watch4);
    }

    @Test
    public void should_find_nothing_when_looking_for_non_existent_items() {
        // given
        var idsOfItemsToFind = List.of(42L, 7L, 100500L);

        // when
        var foundItems = catalogueProvider.findItemsBy(idsOfItemsToFind);

        // then
        assertThat(foundItems).isEmpty();
    }
}
