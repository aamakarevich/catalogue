package me.andrei.catalogue.domain.model;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Discount.singleItemDiscount;
import static me.andrei.catalogue.domain.model.Money.money;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

class CatalogueItemPriceCalculationTest {

    @ParameterizedTest(name = "should_calculate_price_without_discount_{index}()")
    @MethodSource("itemsWithoutDiscountParameters")
    void should_calculate_price_without_discount(int purchasedCount, Money pricePerUnit, Money totalPrice) {
        // given
        var item = new CatalogueItem(1L, "Casio", pricePerUnit);

        // when
        var result = item.priceFor(purchasedCount);

        // then
        assertThat(result).isEqualTo(totalPrice);
    }

    @Test
    void should_calculate_price_without_discount_when_count_is_not_enough() {
        // given
        var item = new CatalogueItem(1, "Casio", money(20), discount(5, money(90)));

        // when
        var result = item.priceFor(4);

        // then
        assertThat(result).isEqualTo(money(20 * 4));
    }

    @Test
    void should_apply_single_item_discount() {
        // given
        var purchasedCount = 20;
        var discountPrice = money(10);
        var item = new CatalogueItem(1, "Casio", money(20), singleItemDiscount(discountPrice));

        // when
        var result = item.priceFor(purchasedCount);

        // then
        assertThat(result).isEqualTo(discountPrice.multiply(purchasedCount));
    }

    @Test
    void should_apply_discount_when_count_is_exactly_enough() {
        // given
        var item = new CatalogueItem(1, "Casio", money(20), discount(5, money(90)));

        // when
        var result = item.priceFor(5);

        // then
        assertThat(result).isEqualTo(money(90));
    }

    @Test
    void should_apply_discount_multiple_times() {
        // given
        var purchasedCount = 20;
        var discountAmount = 5;
        var discountPrice = money(90);
        var item = new CatalogueItem(1, "Casio", money(20), discount(discountAmount, discountPrice));

        // when
        var result = item.priceFor(purchasedCount);

        // then
        assertThat(result).isEqualTo(discountPrice.multiply(purchasedCount / discountAmount));
    }

    @ParameterizedTest(name = "should_apply_discount_and_calculate_usual_price_for_leftover_{index}()")
    @ValueSource(longs = {1, 2, 3, 4})
    void should_apply_discount_and_calculate_usual_price_for_leftover(long leftoverCount) {
        // given
        var item = new CatalogueItem(1, "Casio", money(20), discount(5, money(90)));

        // when
        var result = item.priceFor(10 + leftoverCount);

        // then
        assertThat(result).isEqualTo(money(90 * 2 + 20 * leftoverCount));
    }

    private static Stream<Arguments> itemsWithoutDiscountParameters() {
        return Stream.of(
            arguments(1, money(123_456L), money(123_456L)),
            arguments(2, money(100L), money(200L)),
            arguments(42, money(28L), money(1_176L)),
            arguments(100_500, money(1L), money(100_500L)),
            arguments(11_235_813, money(2L), money(22_471_626L))
        );
    }
}
