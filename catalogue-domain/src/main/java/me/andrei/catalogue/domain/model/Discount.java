package me.andrei.catalogue.domain.model;

import static java.util.Objects.hash;
import static me.andrei.catalogue.common.checks.Checks.checkGreaterThanZero;
import static me.andrei.catalogue.common.checks.Checks.checkRequired;

public class Discount {
    public final long count;
    public final Money price;

    private Discount(long count, Money price) {
        this.count = checkGreaterThanZero("count", count);
        this.price = checkRequired("price", price);
    }

    public static Discount discount(long count, Money price) {
        return new Discount(count, price);
    }

    public static Discount singleItemDiscount(Money price) {
        return new Discount(1, price);
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || getClass() != that.getClass()) return false;
        final var discount = (Discount) that;
        return count == discount.count && price.equals(discount.price);
    }

    @Override
    public int hashCode() {
        return hash(count, price);
    }
}
