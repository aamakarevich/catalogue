package me.andrei.catalogue.domain.provider;

import me.andrei.catalogue.domain.model.CatalogueItem;

import java.util.Collection;
import java.util.List;

public interface CatalogueProvider {

    List<CatalogueItem> findItemsBy(Collection<Long> ids);
}
