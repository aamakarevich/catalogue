package me.andrei.catalogue.domain.model;

import static java.lang.Long.compare;
import static java.util.Objects.hash;
import static me.andrei.catalogue.common.checks.Checks.checkNonNegative;

public class Money implements Comparable<Money> {
    public final long amount;

    private Money(long amount) {
        this.amount = checkNonNegative("amount", amount);
    }

    public static Money money(long amount) {
        return new Money(amount);
    }

    public static Money zero() {
        return new Money(0);
    }

    public Money add(Money money) {
        return new Money(this.amount + money.amount);
    }

    public Money multiply(long times) {
        return new Money(this.amount * times);
    }

    public boolean lessThan(Money that) {
        return compareTo(that) < 0;
    }

    public boolean greaterThan(Money that) {
        return compareTo(that) > 0;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || getClass() != that.getClass()) return false;
        final var money = (Money) that;
        return amount == money.amount;
    }

    @Override
    public int hashCode() {
        return hash(amount);
    }

    @Override
    public String toString() {
        return "money=" + amount;
    }

    @Override
    public int compareTo(Money that) {
        return compare(this.amount, that.amount);
    }
}
