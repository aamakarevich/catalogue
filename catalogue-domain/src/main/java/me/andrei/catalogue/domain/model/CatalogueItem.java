package me.andrei.catalogue.domain.model;

import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static me.andrei.catalogue.common.checks.Checks.checkNotEmpty;
import static me.andrei.catalogue.common.checks.Checks.checkRequired;
import static me.andrei.catalogue.common.checks.Checks.checkThat;

public class CatalogueItem {
    public final long id;
    public final String name;
    public final Money price;
    public final Optional<Discount> discount;

    public CatalogueItem(long id, String name, Money price) {
        this(id, name, price, empty());
    }

    public CatalogueItem(long id, String name, Money price, Discount discount) {
        this(id, name, price, ofNullable(discount));
    }

    public CatalogueItem(long id, String name, Money price, Optional<Discount> discount) {
        this.id = id;
        this.name = checkNotEmpty("name", name);
        this.price = checkRequired("price", price);
        this.discount = checkRequired("discount", discount);
        discount.ifPresent(it ->
            checkThat(it.price.lessThan(price.multiply(it.count)), "discount price should be less than normal price")
        );
    }

    public Money priceFor(long count) {
        return discount.flatMap(discount -> Optional.of(
            discount.price.multiply(count / discount.count)
                .add(price.multiply(count % discount.count)))
        ).orElseGet(() -> price.multiply(count));
    }
}
