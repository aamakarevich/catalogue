package me.andrei.catalogue.domain.provider;

import me.andrei.catalogue.domain.model.CatalogueItem;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toConcurrentMap;
import static java.util.stream.Collectors.toList;
import static me.andrei.catalogue.domain.model.Discount.discount;
import static me.andrei.catalogue.domain.model.Money.money;

public class InMemoryCatalogueProvider implements CatalogueProvider {

    public static final InMemoryCatalogueProvider DEFAULT_IN_MEMORY_CATALOGUE_PROVIDER = new InMemoryCatalogueProvider(List.of(
        new CatalogueItem(1, "Rolex", money(100), discount(3, money(200))),
        new CatalogueItem(2, "Michael Kors", money(80), discount(2, money(120))),
        new CatalogueItem(3, "Swatch", money(50)),
        new CatalogueItem(4, "Casio", money(30))
    ));

    private final Map<Long, CatalogueItem> items;

    public InMemoryCatalogueProvider(List<CatalogueItem> items) {
        this.items = items.stream().collect(toConcurrentMap(item -> item.id, item -> item));
    }

    @Override
    public List<CatalogueItem> findItemsBy(Collection<Long> ids) {
        return items.values().stream()
            .filter(item -> ids.contains(item.id))
            .collect(toList());
    }

    public void clear() {
        this.items.clear();
    }

    public void add(CatalogueItem item) {
        this.items.put(item.id, item);
    }
}
