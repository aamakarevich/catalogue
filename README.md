# Catalogue API #

A simplified e-commerce API with a single endpoint that performs a checkout action.

### API description ###

API contains a single endpoint that performs price calculation for listed catalogue items.

Request

```
POST http://localhost:8080/checkout

# Headers
Accept: application/json
Content-Type: application/json

# Body
[1, 2, 1, 4, 3]
```

Response

```
# Headers
Content-Type: application/json

# Body
{ "amount": 360 }
```

* If some of the items in checkout are not present in catalogue response with status 500 and clarification is returned.
* If there are no items in checkout then 0 price is returned.

### How to build ###

```
./gradlew build
```

### How to run tests ###

`./gradlew test` to run tests

`./gradlew testFunctional` to run functional tests

### How to run ###

```
java -jar catalogue-api/build/libs/catalogue-api*.jar [port]
```

If no port specified then value `8080` will be used by default.

### Considerations and simplifications ###

* To deal with prices `Money` type is introduced. The only field it has is `long amount`. It allows to do all the
  necessary calculations with precision. In real world scenario `Currency` could be introduced as long as desired
  precision for it, still allowing to store both parts (before and after comma) as a long value.
* `CatalogueProvider` is introduced to provide the information about watch catalogue. Simple in memory implementation is
  used in the project, but it can easily be substituted with DB-backed implementation, or remote service call based
  implementation.
* Discount should always provide lower price than price of checking out the same amount of items by price per one unit.
* Discount for 1 item is possible.

### Further improvements ###

* Handle possible overflow of `long amount`
  (as currently maximum count of items in single checkout is not limited).
* If an implementation of `CatalogueProvider` is introduced that requires DB or remote service call some reasonable
  caching mechanism could be applied.

### Frameworks / libraries used ###

* Web-server - javalin (https://javalin.io/)
* Logging - slf4j
* Testing - junit 5, assertj, mockito, unirest
