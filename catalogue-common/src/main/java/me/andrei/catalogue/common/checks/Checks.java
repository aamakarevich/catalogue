package me.andrei.catalogue.common.checks;

import me.andrei.catalogue.common.exception.ValidationException;

import java.util.stream.Stream;

import static java.lang.String.format;

public final class Checks {

    public static <T> T checkRequired(String paramName, T value) {
        validate(value != null, "Required '%s' is missing", paramName);
        return value;
    }

    public static String checkNotEmpty(String paramName, String value) {
        validate(value != null && !value.isBlank(), "%s is empty", paramName);
        return value;
    }

    public static long checkGreaterThanZero(String paramName, long value) {
        validate(value > 0, "%s must be greater than 0, but is %s", paramName, value);
        return value;
    }

    public static long checkNonNegative(String paramName, long value) {
        validate(value >= 0, "%s must be greater than or equal to 0, but is %s", paramName, value);
        return value;
    }

    public static void checkThat(boolean condition, String errorMessage, Object... params) {
        checkState(condition, errorMessage, params);
    }

    public static void checkState(boolean condition, String errorMessage, Object... params) {
        if (!condition)
            throw new IllegalStateException(format(errorMessage, params));
    }

    public static void validate(boolean condition, String errorMessage, Object... params) {
        if (!condition)
            throw new ValidationException(params.length == 0
                ? errorMessage
                : format(errorMessage, Stream.of(params).map(v -> v == null ? "not specified" : v).toArray()));
    }

    private Checks() {
    }
}
